package com.example.juliaset;

import android.graphics.Color;
import android.os.AsyncTask;

public class GetColors extends AsyncTask<String,int[],Void> {
	static int[] defaultColors;
	int[] colors;
	
	static {
       defaultColors = new int[17];
		
		defaultColors[0] = 0xFF000000;
		defaultColors[1] = 0xFF6E00FF;
		defaultColors[2] = 0xFF7800F0;
		defaultColors[3] = 0xFF8200E6;
		defaultColors[4] = 0xFF8C00D6;
		defaultColors[5] = 0xFF9600D2;
		defaultColors[6] = 0xFFA000C8;
		defaultColors[7] = 0xFFAA00BE;
		defaultColors[8] = 0xFFB400B4;
		defaultColors[9] = 0xFFBE00AA;
		defaultColors[10] = 0xFFC800A0;
		defaultColors[11] = 0xFFD20096;
		defaultColors[12] = 0xFFDC008c;
		defaultColors[13] = 0xFFE60082;
		defaultColors[14] = 0xFFF00078;
		defaultColors[15] = 0xFFFF006E;
		defaultColors[16] = 0xFFFF00FF;
		
	}
	
	public int[] getMap(double creal,double cimaginary) {
		
		int[] output = new int[400*400];
		   
		for (int counter=0;counter<400;counter++) {
		      for (int counter1=1;counter1<400;counter1++) {
		         output[counter1*400 + counter] = defaultColors[0];
		      }
		   }
		
		int numberOfIterations = 20;
		
		double infinity = 4;
		
		for (int m=0;m<400;m++) {
			double x0 = -2 + (m*1.0)/(400/(4*1.0));
			
			for (int n=0;n<400;n++) {
				double y0 = 2 - (n*1.0)/(400/(4*1.0));
				
				double q = x0;
				double f = y0;
				int white = 0;
				
				for (int i=0;i<numberOfIterations;i++) {
					double x1 = q*q - f*f + creal;
					double y1 = 2*q*f + cimaginary;
					
					q = x1;
					f = y1;
					
					double z = q*q + f*f;
					
					if (z > infinity) {
						if (i <= 15)
							output[n*400 + m] = colors[i];
						else
							output[n*400 + m] = colors[16];
						
						white = 1;
						
						break;
					}
				}	
			}	
		}

		return(output);
	}
	
	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		int index = 0;
		
		int[] red = new int[17];
		int[] green = new int[17];
		int[] blue = new int[17];
		
		colors = new int[17];
		
		for (int counter=0;counter<params.length-2;counter += 3) {
		   try {
  	          red[index] = Integer.parseInt(params[counter]);
			  green[index] = Integer.parseInt(params[counter+1]);
		      blue[index] = Integer.parseInt(params[counter+2]);
		      
			  colors[index] = Color.rgb(red[index],green[index],blue[index]);
		   } catch (NumberFormatException nfe) {
			  colors[index] = defaultColors[index];
		   }
		   
		   index++;
		}
		
		double c1Real = Double.parseDouble(params[params.length-2]);
		
		double c1Imaginary = Double.parseDouble(params[params.length-1]);
		
		int[] map = getMap(c1Real,c1Imaginary);
		
		publishProgress(map);
		
		return null;
	}
	
	public void onProgressUpdate(int[] ... map) {
		MainActivity.drawingView.setColors(map[0]);
	}

}
