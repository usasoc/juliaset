package com.example.juliaset;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;

public class DrawingView extends View {
	int[] colors;

	public DrawingView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
	}
	
	public void setColors(int[] colors) {
		this.colors = colors;
		
		invalidate();
	}
	
	
	public void onDraw(Canvas canvas) {
	   if (colors != null)
          canvas.drawBitmap(colors, 0, 400,0,0,400,400,true,null);
	}

}
