package com.example.juliaset;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class ControlView extends TableLayout {
	private TextView[] iterations;
	private EditText[] red;
	private EditText[] green;
	private EditText[] blue;
	private Context context;
	
	private TextView[] initialize(TextView[] input) {
		for (int counter=0;counter<input.length;counter++) {
			input[counter] = new TextView(context);
			
			if (counter<17)
				input[counter].setText(String.valueOf(counter));
		}
		
		return(input);
	}
	
	private EditText[] initialize(EditText[] input) {
		for (int counter=0;counter<input.length;counter++)
			input[counter] = new EditText(context);
		
		return(input);
	}
	
	private TextView createTextView(Context context,String label) {
		TextView output = new TextView(context);
		
		output.setText(label);
		
		return(output);
	}

	public ControlView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
		this.context = context;
		
		iterations = new TextView[18];
		
		red = new EditText[18];
		green = new EditText[18];
		blue = new EditText[18];
		
		iterations = initialize(iterations);
		
		red = initialize(red);
		green = initialize(green);
		blue = initialize(blue);
		
		TableRow[] tableRows = new TableRow[20];
		
		for (int counter=0;counter<tableRows.length;counter++)
			tableRows[counter] = new TableRow(context);
		
		TextView iterationsLabel = new TextView(context);
		TextView redLabel = new TextView(context);
		TextView greenLabel = new TextView(context);
		TextView blueLabel = new TextView(context);
		
		iterationsLabel.setText("It.");
		redLabel.setText("Red");
		greenLabel.setText("Green");
		blueLabel.setText("Blue");
		
		TableRow header = new TableRow(context);
		
		header.addView(iterationsLabel);
		header.addView(redLabel);
		header.addView(greenLabel);
		header.addView(blueLabel);
		
		addView(header);
		
		int[] defaultRed = {255,110,120,130,140,150,160,170,180,190,200,210,220,230,240,250,255};
		int[] defaultGreen = {255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		int[] defaultBlue = {255,255,240,230,220,210,200,190,180,170,160,150,140,130,120,110,255};
		
		for (int counter=0;counter<17;counter++) {
		//	red[counter].setLayoutParams(layoutParams);
			
			tableRows[counter].addView(iterations[counter]);
			
		    tableRows[counter].addView(red[counter]);
		    
		    red[counter].setText(String.valueOf(defaultRed[counter]));
		    
		    tableRows[counter].addView(green[counter]);
		    
		    green[counter].setText(String.valueOf(defaultGreen[counter]));
		    
		    tableRows[counter].addView(blue[counter]);
		    
		    blue[counter].setText(String.valueOf(defaultBlue[counter]));
		    
		    addView(tableRows[counter]);
		}
		
		TableRow tableRow = new TableRow(context);
		
		TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
		
		tableRow.addView(createTextView(context,"c"));
		tableRow.addView(createTextView(context,"real"));
		
		final EditText c1Real = new EditText(context);
		
		layoutParams.span = 2;
		
		c1Real.setLayoutParams(layoutParams);
		
		tableRow.addView(c1Real);
		
		addView(tableRow);
		
		tableRow = new TableRow(context);
		
		tableRow.addView(createTextView(context,"c"));
		tableRow.addView(createTextView(context,"imag"));
		
		final EditText c1Imaginary = new EditText(context);
		
		c1Imaginary.setLayoutParams(layoutParams);
		
		tableRow.addView(c1Imaginary);
		
		addView(tableRow);
		
		tableRow = new TableRow(context);
		
		Button draw = new Button(context);

		layoutParams = new TableRow.LayoutParams(200,LayoutParams.MATCH_PARENT);
		
		layoutParams.span = 3;
		
		layoutParams.gravity = Gravity.CENTER;
		
		draw.setText("Draw");
		
		draw.setLayoutParams(layoutParams);
		
		tableRow.addView(draw);
		
		draw.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String[] parameters = new String[53];
				
				int index = 0;
				
				for (int counter=0;counter<17;counter++) {
					parameters[index++] = red[counter].getText().toString();
					parameters[index++] = green[counter].getText().toString();
					parameters[index++] = blue[counter].getText().toString();
				}
				
				parameters[index++] = c1Real.getText().toString();
				parameters[index++] = c1Imaginary.getText().toString();
				
				new GetColors().execute(parameters);				
			}
			
		});
		
		
		addView(tableRow);
		
		
	}

}
