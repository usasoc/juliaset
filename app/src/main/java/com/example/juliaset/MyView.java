package com.example.juliaset;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;

public class MyView extends LinearLayout {

	public MyView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub

		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(400,LayoutParams.MATCH_PARENT);
		
		ControlView controlView = new ControlView(context);
		
		controlView.setLayoutParams(layoutParams);
		
		ScrollView scrollView = new ScrollView(context);
		
		scrollView.addView(controlView);
		
		addView(scrollView);
		
		MainActivity.drawingView = new DrawingView(context);
		
		addView(MainActivity.drawingView);
		
		
	}

}
